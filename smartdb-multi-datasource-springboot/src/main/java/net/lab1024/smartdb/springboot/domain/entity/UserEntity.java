package net.lab1024.smartdb.springboot.domain.entity;

import net.lab1024.smartdb.annotation.PrimaryKey;
import net.lab1024.smartdb.annotation.TableAlias;
import net.lab1024.smartdb.annotation.UseGeneratedKey;
import net.lab1024.smartdb.springboot.constants.UserLevelEnum;

import java.time.LocalDateTime;

/**
 * @author zhuoda
 */

@TableAlias("t_user")
public class UserEntity {

    @PrimaryKey
    @UseGeneratedKey
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 登录时间
     */
    private LocalDateTime loginTime;

    /**
     * 1男，0女
     */
    private Integer sex;

    /**
     * 等级：vip 或者 plus
     */
    private UserLevelEnum level;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(LocalDateTime loginTime) {
        this.loginTime = loginTime;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public UserLevelEnum getLevel() {
        return level;
    }

    public void setLevel(UserLevelEnum level) {
        this.level = level;
    }
}
