package net.lab1024.smartdb.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartDbMultiDataSourceSpringBootDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartDbMultiDataSourceSpringBootDemoApplication.class, args);
	}

}
