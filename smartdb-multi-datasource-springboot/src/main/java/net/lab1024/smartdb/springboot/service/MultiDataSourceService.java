package net.lab1024.smartdb.springboot.service;

import net.lab1024.smartdb.SmartDb;
import net.lab1024.smartdb.springboot.domain.entity.UserEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 读写分离 demos
 *
 * @author zhuoda
 */

@Service
public class MultiDataSourceService {

    /**
     * 第三方数据库
     */
    @Resource(name = "third")
    private SmartDb thirdSmartDb;

    /**
     * 主、从数据库
     */
    @Resource(name = "default")
    private SmartDb defaultSmartDb;


    /**
     * 操作多个数据源<br>
     * 多个数据源在一个方法中操作
     *
     * @return
     */
    public List<UserEntity> multiDataSourceOperate() {
        /**
         * 从第三方数据库读取数据.
         */
        List<UserEntity> list = thirdSmartDb.selectSqlBuilder()
                .select("*")
                .from(UserEntity.class)
                .queryList(UserEntity.class);

        /**
         * 将数据写入到主库
         */
        defaultSmartDb.batchInsert(list);
        return list;
    }

}
