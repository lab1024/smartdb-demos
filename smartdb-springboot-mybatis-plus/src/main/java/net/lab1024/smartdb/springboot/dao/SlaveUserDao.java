package net.lab1024.smartdb.springboot.dao;

import net.lab1024.smartdb.SmartDb;
import net.lab1024.smartdb.springboot.domain.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zhuoda
 * @Date 2020/9/16
 */
@Repository
public class SlaveUserDao {

    @Autowired
    private SmartDb smartDb;


    public List<UserEntity> selectUser() {
        return smartDb.queryList(UserEntity.class, "select * from t_user");
    }

}
