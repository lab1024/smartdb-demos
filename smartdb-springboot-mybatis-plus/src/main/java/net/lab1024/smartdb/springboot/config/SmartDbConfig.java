package net.lab1024.smartdb.springboot.config;

import net.lab1024.smartdb.SmartDb;
import net.lab1024.smartdb.SmartDbBuilder;
import net.lab1024.smartdb.database.SupportDatabaseType;
import net.lab1024.smartdb.ext.SmartDbExtEnum;
import net.lab1024.smartdb.filter.impl.SqlExecutorTimeFilter;
import net.lab1024.smartdb.sqlbuilder.convertor.CaseFormatColumnNameConverter;
import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;
import com.google.common.base.CaseFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * 此SmartDb 使用的是slave数据源
 *
 * @author zhuoda
 */
@Configuration
public class SmartDbConfig {

    @Value("${slave.spring.datasource.driver-class-name}")
    private String driver;

    @Value("${slave.spring.datasource.url}")
    private String url;

    @Value("${slave.spring.datasource.username}")
    private String username;

    @Value("${slave.spring.datasource.password}")
    private String password;

    @Value("${slave.spring.datasource.initial-size}")
    private Integer initialSize;

    @Value("${slave.spring.datasource.min-idle}")
    private Integer minIdle;

    @Value("${slave.spring.datasource.max-active}")
    private Integer maxActive;

    @Value("${slave.spring.datasource.max-wait}")
    private Long maxWait;

    @Value("${slave.spring.datasource.time-between-eviction-runs-millis}")
    private Long timeBetweenEvictionRunsMillis;

    @Value("${slave.spring.datasource.min-evictable-edle-time-millis}")
    private Long minEvictableIdleTimeMillis;

    @Value("${slave.spring.datasource.filters}")
    private String filters;

    @Bean
    public SmartDb initSmartDb() throws SQLException {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(driver);
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);
        druidDataSource.setInitialSize(initialSize);
        druidDataSource.setMinIdle(minIdle);
        druidDataSource.setMaxActive(maxActive);
        druidDataSource.setMaxWait(maxWait);
        druidDataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        druidDataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        druidDataSource.setValidationQuery("SELECT 1 FROM dual");
        druidDataSource.setFilters(filters);
        ArrayList<Filter> arrayList = new ArrayList<>();
        druidDataSource.setProxyFilters(arrayList);
        druidDataSource.init();
        SmartDb smartDb = SmartDbBuilder.create()
                // 设置 数据库 连接池（数据源）
                .setMasterDataSource(druidDataSource)
                // 打印 info 级别sql
                .setShowSql(true)
                // 设置数据库类型
                .setSupportDatabaseType(SupportDatabaseType.MYSQL)
                // 设置支持spring 5
                .setSmartDbExtEnum(SmartDbExtEnum.SPRING5)
                //表名与类名转换
                .setTableNameConverter(cls -> "t_" + CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, cls.getSimpleName()))
                //列名字 转换
                .setColumnNameConverter(new CaseFormatColumnNameConverter(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE))
                //添加 sql 执行时间的 debug 信息filter
                .addSmartDbFilter(new SqlExecutorTimeFilter())
                .build();
        return smartDb;
    }

}
