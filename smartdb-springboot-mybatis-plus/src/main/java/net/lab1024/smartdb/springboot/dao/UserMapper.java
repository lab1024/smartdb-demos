package net.lab1024.smartdb.springboot.dao;

import net.lab1024.smartdb.springboot.domain.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author zhuoda
 */

@Mapper
@Component
public interface UserMapper extends BaseMapper<UserEntity>{


}
