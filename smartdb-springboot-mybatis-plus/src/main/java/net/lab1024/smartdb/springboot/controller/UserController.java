package net.lab1024.smartdb.springboot.controller;

import net.lab1024.smartdb.springboot.service.UserService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhuoda
 */

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/test")
    public String insert() {
        return JSONObject.toJSONString(userService.test());
    }


}
