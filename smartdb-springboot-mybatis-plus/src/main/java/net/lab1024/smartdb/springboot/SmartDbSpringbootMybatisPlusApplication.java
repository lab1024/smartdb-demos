package net.lab1024.smartdb.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("net.lab1024.smartdb.springboot.*")
public class SmartDbSpringbootMybatisPlusApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartDbSpringbootMybatisPlusApplication.class, args);
	}

}
