package net.lab1024.smartdb.springboot.dao;

import cn.zhuoluodada.opensource.smartdb.ext.spring.SmartDbDaoSupport;
import net.lab1024.smartdb.springboot.domain.entity.UserEntity;
import org.springframework.stereotype.Repository;

/**
 * @author zhuoda
 */

@Repository
public class UserDao extends SmartDbDaoSupport<UserEntity> {

    public UserEntity selectByUsernameAndPassword(String username, String password) {
        return smartDb.selectSqlBuilder()
                .select("*")
                .from(entityClass)
                .whereAnd("username = ?", username)
                .whereAnd("password = ?", password)
                .queryFirst(entityClass);
    }


}
