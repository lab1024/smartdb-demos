package net.lab1024.smartdb.springboot.controller;

import net.lab1024.smartdb.springboot.constants.UserLevelEnum;
import net.lab1024.smartdb.springboot.domain.entity.UserEntity;
import net.lab1024.smartdb.springboot.service.UserService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author zhuoda
 */

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/insert")
    public String insert() {
        UserEntity user = new UserEntity();
        user.setUsername("user_" + ThreadLocalRandom.current().nextInt(1000, 9999));
        user.setLevel(UserLevelEnum.VIP);
        user.setPassword(String.valueOf(ThreadLocalRandom.current().nextInt(1000, 9999)));
        user.setSex(1);
        user.setLoginTime(LocalDateTime.now());
        UserEntity result = userService.insert(user);
        return JSONObject.toJSONString(result);
    }
    @GetMapping("/user/testTransaction")
    public String testTransaction() {
        userService.testTransaction();
        return "success";
    }

    @GetMapping("/user/query")
    public String query() {
        return "<pre>" + JSONObject.toJSONString(userService.queryList(),true)+"</pre>";
    }

    @GetMapping("/user/batchInsert")
    public String batchInsert() {
        return "<pre>" + JSONObject.toJSONString(userService.batch(),true)+"</pre>";
    }

    @GetMapping("/user/delete/{id}")
    public String delete(@PathVariable Integer id) {
        int rows = userService.deleteById(id);
        return "成功删除:  " + rows + " 条数据!";
    }
    @GetMapping("/user/deleteAll")
    public String deleteAll() {
        int rows = userService.deleteAll();
        return "成功删除:  " + rows + " 条数据!";
    }


}
