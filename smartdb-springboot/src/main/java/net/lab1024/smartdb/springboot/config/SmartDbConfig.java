package net.lab1024.smartdb.springboot.config;

import cn.zhuoluodada.opensource.smartdb.SmartDb;
import cn.zhuoluodada.opensource.smartdb.SmartDbBuilder;
import cn.zhuoluodada.opensource.smartdb.database.SupportDatabaseType;
import cn.zhuoluodada.opensource.smartdb.ext.SmartDbExtEnum;
import cn.zhuoluodada.opensource.smartdb.filter.impl.SqlExecutorTimeFilter;
import cn.zhuoluodada.opensource.smartdb.sqlbuilder.convertor.CaseFormatColumnNameConverter;
import com.alibaba.druid.pool.DruidDataSource;
import com.google.common.base.CaseFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * smart db 配置
 *
 * @author zhuoda
 */
@Configuration
public class SmartDbConfig {

    @Autowired
    private DruidDataSource dataSource;

    @Bean
    public SmartDb initSmartDb() {
        SmartDb smartDb = SmartDbBuilder.create()
                // 设置 数据库 连接池（数据源）
                .setMasterDataSource(dataSource)
                // 打印 info 级别sql
                .setShowSql(true)
                // 设置数据库类型
                .setSupportDatabaseType(SupportDatabaseType.MYSQL)
                // 设置支持spring 5
                .setSmartDbExtEnum(SmartDbExtEnum.SPRING5)
                //表名与类名转换
                .setTableNameConverter(cls -> "t_" + CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, cls.getSimpleName()))
                //列名字 转换
                .setColumnNameConverter(new CaseFormatColumnNameConverter(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE))
                //添加 sql 执行时间的 debug 信息filter
                .addSmartDbFilter(new SqlExecutorTimeFilter())
                .build();
        return smartDb;
    }

}
