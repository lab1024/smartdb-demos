package net.lab1024.smartdb.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartDbSpringbootDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartDbSpringbootDemoApplication.class, args);
	}

}
