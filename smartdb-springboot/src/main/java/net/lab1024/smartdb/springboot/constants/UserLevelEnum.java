package net.lab1024.smartdb.springboot.constants;

/**
 * @author zhuoda
 */
public enum UserLevelEnum {

    VIP,
    PLUS
}
