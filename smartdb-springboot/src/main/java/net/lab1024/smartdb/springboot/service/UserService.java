package net.lab1024.smartdb.springboot.service;

import cn.zhuoluodada.opensource.smartdb.pagination.PaginateResult;
import cn.zhuoluodada.opensource.smartdb.pagination.SmartDbPaginateParam;
import net.lab1024.smartdb.springboot.constants.UserLevelEnum;
import net.lab1024.smartdb.springboot.dao.UserDao;
import net.lab1024.smartdb.springboot.domain.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author zhuoda
 */

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public PaginateResult<UserEntity> queryList() {
        SmartDbPaginateParam param = new SmartDbPaginateParam();
        param.setSearchCount(true);
        param.setPageSize(Short.MAX_VALUE);
        param.setPageNumber(1);
        return userDao.paginate(param);
    }

    //登录操作
    public UserEntity login(String username, String password) {
        UserEntity userEntity = userDao.selectByUsernameAndPassword(username, password);
        return userEntity;
    }

    /**
     * 只需要在创建 “SmartDb”的时 设置支持spring <br>
     * SmartDbBuilder.setSmartDbExtEnum(SmartDbExtEnum.SPRING4) <br>
     * 则就可以支持Spring的事务
     *
     * @author : zhuoda
     */
    @Transactional(rollbackFor = Exception.class)
    public void testTransaction() {
        UserEntity user = new UserEntity();
        user.setUsername("user_" + ThreadLocalRandom.current().nextInt(1000, 9999));
        user.setLevel(UserLevelEnum.VIP);
        user.setSex(1);
        user.setLoginTime(LocalDateTime.now());
        userDao.insert(user);

        System.out.println(1 / 0);

        UserEntity user2 = new UserEntity();
        user2.setUsername("user_" + ThreadLocalRandom.current().nextInt(1000, 9999));
        user2.setLevel(UserLevelEnum.PLUS);
        user2.setSex(1);
        user2.setLoginTime(LocalDateTime.now());
        userDao.insertSelective(user);
    }

    public List<UserEntity> batch() {
        List<UserEntity> userList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserEntity user = new UserEntity();
            user.setUsername("user_" + ThreadLocalRandom.current().nextInt(1000, 9999));
            user.setPassword("pwd_" + ThreadLocalRandom.current().nextInt(1000, 9999));
            user.setLevel(UserLevelEnum.VIP);
            user.setSex(1);
            user.setLoginTime(LocalDateTime.now());
            userList.add(user);
        }
        return userDao.batchInsert(userList);
    }

    public UserEntity insert(UserEntity user) {
        return userDao.insert(user);
    }

    public void insertSelective(UserEntity user) {
        userDao.insertSelective(user);
    }

    public void update(UserEntity user) {
        userDao.update(user);
    }

    public void updateSelective(UserEntity user) {
        userDao.updateSelective(user);
    }

    public void delete(UserEntity user) {
        userDao.delete(user);
    }

    public Integer deleteById(Integer id) {
        return userDao.deleteByPrimaryKey(id);
    }


    public int deleteAll() {
        return userDao.deleteAll();
    }
}
