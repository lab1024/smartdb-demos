### SmartDb官方Demos

#### 1、smartdb-springboot 

SmartDb和SpringBoot集成和一些 样例代码

#### 2、smartdb-springboot-mybatis-plus 

SmartDb和SpringBoot + Mybatis 集成和一些 样例代码

#### 3、smartdb-multi-datasource-springboot 

SmartDb解决 SpringBoot 多数据库、多数据源 集成和一些 样子代码

### SmartDb文档

github:  [https://github.com/zhuoluodada/smartdb](https://github.com/zhuoluodada/smartdb "https://github.com/zhuoluodada/smartdb")

gitee:  [https://gitee.com/zhuoluodada/smartdb](https://gitee.com/zhuoluodada/smartdb "https://gitee.com/zhuoluodada/smartdb")

官方文档：[https://zhuoluodada.cn/article/page/articleList?catalogId=7](https://zhuoluodada.cn/article/page/articleList?catalogId=7 "https://zhuoluodada.cn/article/page/articleList?catalogId=7")

---
#### 作者

[卓大](https://zhuoluodada.cn)， 1024创新实验室主任，混迹于各个技术圈，研究过计算机，熟悉点java，略懂点前端。


**SmartDb 微信群（加我微信拉你入群！）**

#### 捐赠
开源不易，感谢捐赠
>*佛祖保佑捐赠这些人写程序永无bug，工资翻倍，迎娶白富美，走上人生巅峰！*

![](https://images.gitee.com/uploads/images/2020/0416/190000_cb8c44cd_5589720.jpeg)
