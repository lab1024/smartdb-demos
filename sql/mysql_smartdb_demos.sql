-- --------------------------------------------------------
-- 主机:                           148.70.90.84
-- 服务器版本:                        8.0.16 - MySQL Community Server - GPL
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 smartdb_demo_master 的数据库结构
DROP DATABASE IF EXISTS `smartdb_demo_master`;
CREATE DATABASE IF NOT EXISTS `smartdb_demo_master` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `smartdb_demo_master`;

-- 导出  表 smartdb_demo_master.t_user 结构
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `username` varchar(200) DEFAULT NULL COMMENT '用户名',
  `password` varchar(200) DEFAULT NULL COMMENT '密码',
  `sex` tinyint(4) DEFAULT NULL COMMENT '1男，0女',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `level` varchar(50) DEFAULT NULL COMMENT '等级：vip 或者 plus',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- 正在导出表  smartdb_demo_master.t_user 的数据：~1 rows (大约)
DELETE FROM `t_user`;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` (`id`, `username`, `password`, `sex`, `login_time`, `level`) VALUES
	(85, 'master_user_6274', NULL, 1, '2020-09-16 20:17:21', 'VIP');
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;


-- 导出 smartdb_demo_slave 的数据库结构
DROP DATABASE IF EXISTS `smartdb_demo_slave`;
CREATE DATABASE IF NOT EXISTS `smartdb_demo_slave` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `smartdb_demo_slave`;

-- 导出  表 smartdb_demo_slave.t_user 结构
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `username` varchar(200) DEFAULT NULL COMMENT '用户名',
  `password` varchar(200) DEFAULT NULL COMMENT '密码',
  `sex` tinyint(4) DEFAULT NULL COMMENT '1男，0女',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `level` varchar(50) DEFAULT NULL COMMENT '等级：vip 或者 plus',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- 正在导出表  smartdb_demo_slave.t_user 的数据：~1 rows (大约)
DELETE FROM `t_user`;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` (`id`, `username`, `password`, `sex`, `login_time`, `level`) VALUES
	(85, 'slave_user_6274', NULL, 1, '2020-09-16 20:17:21', 'VIP');
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;


-- 导出 smartdb_demo_third 的数据库结构
DROP DATABASE IF EXISTS `smartdb_demo_third`;
CREATE DATABASE IF NOT EXISTS `smartdb_demo_third` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `smartdb_demo_third`;

-- 导出  表 smartdb_demo_third.t_user 结构
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `username` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `sex` tinyint(4) DEFAULT NULL COMMENT '1男，0女',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `level` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '等级：vip 或者 plus',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- 正在导出表  smartdb_demo_third.t_user 的数据：~0 rows (大约)
DELETE FROM `t_user`;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
